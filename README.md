# Facebook Rules scripts

### Priprava Business računa:
* za vsak business account je potrebno narediti ``Developer APP`` (preko https://business.facebook.com/settings/apps/),
* dodati ``Marketing API`` pod ``Products`` APP-a,
* kreirati ``System Userja`` z imenom ``API`` (https://business.facebook.com/settings/system-users/) in ``Admin access`` levelom. ``Employee access`` ni dovolj..
* Dodati ``Assete`` temu userju (vse Ad accounte, katere bo spreminjal in dostop do zgornjega APP-a)


System user mora imeti sledeče permissione: ``pages_show_list``, ``ads_read``, ``ads_management``

#### Autentikacija

Pogledaš v ``RulesExecution.php`` in popraviš constante.

#### Uporaba

Na sistemu more bit nameščen PHP 7.4+ in Composer.

* ``cd FacebookSKD/``
* ``composer install``
* Popraviš loop variable če je potrebno (``IncreaseCPAClass.php``, ``DecreaseCPAClass.php``)
* Dodaš authentication variable  (``RulesExecution.php``) (glej komentarje)
* poženeš skripto z ``php RulesExecution.php``

#### Uradna dokumentacija
Na tem url-ju je cel kup guidov, odvisno kakšna pravila bi bilo potrebno narediti.
* Ad Rules Engine: https://developers.facebook.com/docs/marketing-api/ad-rules

##### Condition specifications
* Evaluation specs: https://developers.facebook.com/docs/marketing-api/ad-rules/overview/evaluation-spec
* Execution specs: https://developers.facebook.com/docs/marketing-api/ad-rules/overview/execution-spec
* Changes specs: https://developers.facebook.com/docs/marketing-api/ad-rules/overview/change-spec

##### Developer Tools

GraphQL API explorer, Access token debugger ipd.: https://developers.facebook.com/tools/